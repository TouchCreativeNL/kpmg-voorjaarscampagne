const express = require('express')

const uploadController = require('../controllers/uploadController')

const router = express.Router()

router.route('/').post(uploadController.getUpload)

// router.post('/upload', function (req, res, next) {
// const body = req.body
// console.log(body)
// upload(req, res, function () {
//   try {
//     console.log(req)
//   } catch (err) {
//     console.log(err)
//   }
// })
// res.json({ file: req.file })
// next()
// upload(req, res, function (err) {
//   console.log(req.body.filename)
//   if (err) {
//     res.json({ error_code: 1, err_desc: err })
//     return
//   }
//   res.json({ error_code: 0, err_desc: null })
// })
// console.log(req.body.fname)
// res.json({ file: req.file })
// })

// router.use(function (err, req, res, next) {
//   if (err.code === 'LIMIT_FILE_TYPES') {
//     return res.status(422).json({ error: 'Only images allowed!' })
//   }

//   if (err.code === 'LIMIT_FILE_SIZE') {
//     return res.status(422).json({ error: 'Too large...' })
//   }
// })

module.exports = router
