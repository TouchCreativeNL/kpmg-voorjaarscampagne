const express = require('express')
const cors = require('cors')
const app = express()

const uploadRoutes = require('./routes/uploadRoutes')
const mailRoutes = require('./routes/mailRoutes')

app.use(express.json())
app.use(cors())

app.use('/api/upload', uploadRoutes)
app.use('/api/mail', mailRoutes)

module.exports = app

// Start standalone server if directly running
if (require.main === module) {
  const port = process.env.PORT || 3001
  app.listen(port, () => {
    // eslint-disable-next-line no-console
    console.log(`API server listening on port ${port}`)
  })
}
