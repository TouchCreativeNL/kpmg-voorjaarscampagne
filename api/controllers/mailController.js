/* eslint-disable node/no-path-concat */
const nodemailer = require('nodemailer')

const sendMail = (msgObj) => {
  let msg = ''
  msg += 'Naam: ' + msgObj.fileName + '<br/>'
  msg += '<img src="cid:unique@kreata.ee"/>'
  const transporter = nodemailer.createTransport({
    host: 'srv20286.flexwebhosting.nl',
    port: 465,
    secure: false,
    // auth: {
    //   user: 'dave@touchcreative.nl',
    //   pass: 'Amsterdam23!',
    // },
    tls: {
      rejectUnauthorized: false,
    },
  })
  transporter.sendMail(
    {
      from: msgObj.email,
      to: msgObj.hisEmail,
      subject: `This card is for ${msgObj.fileName}!`,
      html: msg,
      attachments: [
        {
          filename: 'image.png',
          path: __dirname + `/uploads/${msgObj.fileName}.jpg`,
          cid: 'unique@kreata.ee',
        },
      ],
    },
    (error, info) => {
      if (error) {
        return console.log(error)
      }
      console.log('sent message')
    }
  )
}

exports.sendMail = (req, res) => {
  const msgObj = req.body
  console.log('Sending email: ', msgObj)
  sendMail(msgObj)
  res.status(200).json({ message: 'success' })
}
