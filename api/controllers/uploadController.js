const fs = require('fs')
const multer = require('multer')

const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, './api/controllers/uploads')
  },
  filename(req, file, cb) {
    cb(null, file.originalname)
  },
})

const upload = multer({ storage }).single('file')

exports.getUpload = (req, res) => {
  upload(req, res, function (err) {
    if (err) {
      console.log(err)
    }

    fs.renameSync(
      req.file.path,
      req.file.path.replace(
        `api/controllers/uploads/${req.file.filename}`,
        `api/controllers/uploads/${req.body.fileName}.jpg`
      )
    )
  })
}
