import Vue from 'vue'
import Vuetify from 'vuetify'


Vue.use(Vuetify)
const vuetify = new Vuetify({
    theme: { light: true, primary: '#fff' },
})

export default vuetify
